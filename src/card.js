import React from 'react';
import './card.css';
import avatar from './images/avatar.png'
import back from './images/back.png'
import clapping from './images/clapping.png'
import phone from './images/phone.png'
import line from './images/Line 1.png'

class Card extends React.Component{
    render(){
        return(
        <div className="head">
            <div>
                <h1>Review Card Component</h1>
                <img src={line}/>
            </div>
            <div className="main">
                <div className="headder">
                {this.props.heading}
                </div>


               
                <div className="userdata">
                <div className="user">
                    <img src={avatar} className="avatar"/>
                </div>

                <div className="username">
                {this.props.userName}
                </div>
                <div className="reviews">
                {this.props.userReviews}
                </div>
                </div>

                <div className="floated">

                <div className="views"> {this.props.views}</div>
                </div>
                
                <div className="content">
                <p> With the OnePlus 3T, we got virtually the same body as the OnePlus 3,
                    but packed with a new processor, more RAM, and a bigger battery. yet,
                    and the 6-8 GB of RAM offered in the OnePlus 5 was already the highest
                    the industry had ever seen. <span><a href="#">(readmore)</a></span></p>
                </div>

                <div className="user">
                    <img src={back} className="back"/>
                </div>

                <div className="footer">
                    <img src={clapping} className="icon"/>
                </div>
                <div className="floated-next">

        <div className="views"> {this.props.clapping}</div>
                </div>
                
                <div className="floated">

                <div className="first">
                <div className="info"> 
                <img src={phone} className="phone"/>
                </div>
                </div>
                <div className="second">
                <div className="username1">
                {this.props.mobile}
                </div>
                <div className="reviews1">
                {this.props.reviews}
                </div>
                </div>
                
                </div>

                
                

    
            </div>
        </div>
            
        );
    }
}

export default Card;