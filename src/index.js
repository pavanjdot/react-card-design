import ReactDom from 'react-dom'
import React from 'react'
import App from './App'
import './fonts/AvenirLTStd-Book.otf'

ReactDom.render(<App/>, document.getElementById("root"));