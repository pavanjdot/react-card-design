import React from 'react';
import Card from './card';

class App extends React.Component{
    //Intitilizing State, Default values for demo.
    state = {
        heading : "It’s all about that amazing Screen and Battery",
        userName : "Subham Agarwal",
        userReviews : "12k Reviews",
        views : "234 Views",
        clapping : "23",
        mobile : "OnePlus 6T", 
        reviews : "1.4k reviews",

    };
//Adiing New Values through SetState, Data from the API.
    // componentDidMount(){
    //     this.setState = {
    //         heading : "It’s all about that amazing Screen and Battery",
    //         userName : "Subham Agarwal",
    //         userReviews : "12k Reviews",
    //         views : "234 Views",
    //         clapping : "23",
    //         mobile : "OnePlus 6T", 
    //         reviews : "1.4k reviews",
    //     }
    // }
   


    render(){
        return(
            <div>
                {/* Calling Card Component here, passing props*/}
                <Card heading = {this.state.heading}
                      userName = {this.state.userName}
                      userReviews = {this.state.userReviews}
                      views = {this.state.reviews}
                      clapping = {this.state.clapping}
                      mobile = {this.state.mobile}
                      reviews = {this.state.reviews}/>
            </div>
        );
    }
}

export default App;